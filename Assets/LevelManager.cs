﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

public class LevelManager : MonoBehaviour {
    Coroutine activeCor;
    public GameObject loadingScreen, purchaseScreen;
    GameObject levelGenerator;
    GameObject player;
    public int seed = 0;
    public bool playerActive;
    public static LevelManager instance;
    public int wallLength = 8;
	// Use this for initialization
	void Start () {
        StartCoroutine(InitialSetup());
	}

    IEnumerator InitialSetup()
    {
        levelGenerator = GameObject.Find("LevelGenerator");
        if (GameObject.Find("Player") == null)
        {
            SceneManager.LoadSceneAsync("player", LoadSceneMode.Additive);
        }
        
        loadingScreen.SetActive(false);
        ChangeLevel("leveltest");
        instance = this;
        yield return new WaitForFixedUpdate();
    }

    public void ChangeLevel(string name)
    {
        if (activeCor == null)
        {
            activeCor = StartCoroutine(LoadLevel(name));
        }
    }

    IEnumerator LoadLevel(string name)
    {
        loadingScreen.SetActive(true);
        switch (name) 
        {
            case "leveltest":
                seed++;
                yield return new WaitForFixedUpdate();
                SceneManager.UnloadScene(name);
                yield return new WaitForFixedUpdate();
                AsyncOperation a = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
                yield return new WaitUntil(delegate() { return a.isDone; });
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(name));
                levelGenerator = GameObject.Find("LevelGenerator");
                GenerateLevel(levelGenerator.GetComponent<NodeGraphStructureGeneration>(), wallLength);
                GameObject.Find("Player").transform.position = GameObject.Find("PlayerSpawn").transform.position + Vector3.up / 2;
                GameObject.Find("CameraOrigin").SendMessage("SnapToPlayer");
                activeCor = null;
                RenderSettings.fogColor = Color.grey;
                break;
        }
        yield return new WaitForFixedUpdate();
        loadingScreen.SetActive(false);
        purchaseScreen.SetActive(true);
    }

    public void SetPlayerActive(bool state, float delay = 0f)
    {
        StartCoroutine(CSetPlayerActive(state, delay));
    }

    IEnumerator CSetPlayerActive(bool state, float delay)
    {
        yield return new WaitForSeconds(delay);
        playerActive = state;
    }

    void GenerateLevel(NodeGraphStructureGeneration generator, int wallLength)
    {
        generator.noiseSeed = seed;
        List<Building> buildings = PositionBuildings(generator);
        List<Building> catwalks = new List<Building>();
        buildings.ForEach(x => { x.CreateRaycastedCatwalks(buildings, catwalks, generator); });
        GameObject.Find("PlayerSpawn").transform.position = buildings[0].position + new Vector3(5, 0, 5);
        buildings.ForEach(x => { generator.InstantiateWalls(x); });
        catwalks.ForEach(x => { generator.InstantiateWalls(x); });
    }

    List<Building> PositionBuildings(NodeGraphStructureGeneration generator)
    {
        List<Building> buildings = new List<Building>();
        int buildingHeight = 10;
        int buildingWidth = 10;
        int buildingOffset = 5;
        for (int i = 0; i < 10; i++)
        {
            Building b = generator.Generate(new Vector3(0, 0, i * (buildingHeight + buildingOffset)) * wallLength, 10, buildingHeight, wallLength);
            buildings.Add(b);
            int raycastStartPoint = Mathf.RoundToInt(Mathf.Lerp(0, b.levelWidth - 2, Random.value));
            b.exits.Add(b.GetWallAt(raycastStartPoint, b.levelHeight - 1, false));
            generator.noiseSeed += 1;
        }
        for (int i = 1; i < 10; i++)
        {
            Building b = generator.Generate(new Vector3(i * (buildingWidth + buildingOffset), 0, 0) * wallLength, 10, buildingWidth, wallLength);
            buildings.Add(b);
            int raycastStartPoint = Mathf.RoundToInt(Mathf.Lerp(0, b.levelHeight - 2, Random.value));
            b.exits.Add(b.GetWallAt(b.levelWidth - 1, raycastStartPoint, true));
            generator.noiseSeed += 1;
        }
        
        return buildings;
    }
}

public class Building
{
    public Vector3 position;
    public int levelWidth, levelHeight, wallLength;
    public List<Wall> walls;
    public List<Wall> exits;

    public Building(Vector3 position, int levelWidth, int levelHeight, int wallLength)
    {
        this.position = position;
        this.levelWidth = levelWidth;
        this.levelHeight = levelHeight;
        this.wallLength = wallLength;
        exits = new List<Wall>();
    }

    public bool Contains(Vector3 point)
    {
        return (position.x <= point.x && 
            position.z <= point.z && 
            position.x + levelWidth * wallLength >= point.x && 
            position.z + levelHeight * wallLength >= point.z);
    }

    public Vector3 TopLeft()
    {
        return position + new Vector3(0, 0, (levelHeight - 1) * wallLength);
    }

    public Vector3 BottomLeft()
    {
        return position;
    }

    public Vector3 BottomRight()
    {
        return position + new Vector3((levelWidth - 1) * wallLength, 0, 0);
    }

    public Vector3 TopRight()
    {
        return position + new Vector3((levelWidth - 1) * wallLength, 0, (levelHeight - 1) * wallLength);
    }

    public void CreateRaycastedCatwalks(List<Building> buildings, List<Building> catwalks, NodeGraphStructureGeneration generator)
    {
        BuildingRaycastHit hit;
        foreach (Wall exit in exits)
        {
            if (!exit.connected)
            {
                Vector3 raycastStartPoint = exit.p1 * wallLength + position;
                if (BuildingRaycast(raycastStartPoint, exit.vertical ? Vector3.right : Vector3.forward, 9999, buildings, out hit, this))
                {
                    Building newCatwalk;
                    if (exit.vertical)
                    {
                        newCatwalk = generator.Generate(raycastStartPoint, Mathf.RoundToInt(Vector3.Distance(raycastStartPoint, hit.point) / wallLength) + 1, 2, wallLength, true);
                        newCatwalk.exits.Add(newCatwalk.GetWallAt(0, 0, true));
                        newCatwalk.exits.Add(newCatwalk.GetWallAt(0, newCatwalk.levelWidth - 1, true));
                    }
                    else
                    {
                        newCatwalk = generator.Generate(raycastStartPoint, 2, Mathf.RoundToInt(Vector3.Distance(raycastStartPoint, hit.point) / wallLength) + 1, wallLength, true);
                        newCatwalk.exits.Add(newCatwalk.GetWallAt(0, 0, false));
                        newCatwalk.exits.Add(newCatwalk.GetWallAt(0, newCatwalk.levelHeight - 1, false));
                    }
                    catwalks.Add(newCatwalk);
                    
                    exit.connected = true;
                    hit.wall.connected = true;
                    hit.building.exits.Add(hit.wall);
                }
            }
        }
    }

    public static bool BuildingRaycast(Vector3 startPosition, Vector3 direction, int length, List<Building> buildings, out BuildingRaycastHit hit, Building ignore = null)
    {
        hit = new BuildingRaycastHit();

        for (int i = 0; i < length; i++)
        {
            Vector3 p = Vector3.Lerp(startPosition, startPosition + direction * length, (float)i / (float)length);
            foreach (Building b in buildings)
            {
                if ((ignore == null || b != ignore) && b.Contains(p))
                {
                    hit.point = p;
                    hit.building = b;
                    Vector3 wallPosition = (p - b.position).Rounded() / b.wallLength;
                    bool vertical = Mathf.Min(p.x - b.position.x, b.position.x + b.levelWidth * b.wallLength - p.x) >= Mathf.Min(p.z, b.position.z, p.z, b.position.z + b.levelHeight * b.wallLength - p.z);
                    Debug.Log(p);
                    //Debug.Log(vertical + " " + Mathf.Min(p.x - b.position.x, b.position.x + b.levelWidth * b.wallLength - p.x) + " " + Mathf.Min(p.z, b.position.z, p.z, b.position.z + b.levelHeight * b.wallLength - p.z));
                    hit.wall = b.GetWallAt(Mathf.RoundToInt(wallPosition.x), Mathf.RoundToInt(wallPosition.z), vertical);
                    return true;
                }
            }
        }
        hit = null;
        return false;
    }

    public Wall GetWallAt(int xPosition, int zPosition, bool vertical) 
    {
        if (vertical)
        {
            return walls[(zPosition + xPosition * levelHeight) * 2 + 1];
        }
        else
        {
            return walls[(zPosition + xPosition * levelHeight) * 2];
        }
    }
}

public class BuildingRaycastHit
{
    public Vector3 point;
    public Building building;
    public Wall wall;
}