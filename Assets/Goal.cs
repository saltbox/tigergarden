﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {
    void OnTriggerEnter(Collider c)
    {
        if (c.tag == "Player")
        {
            LevelManager.instance.SetPlayerActive(false);
            GameObject.Find("GameStateManager").GetComponent<LevelManager>().ChangeLevel("leveltest");
        }
    }
}
