﻿using UnityEngine;
using System.Collections;
using FMODUnity;
using System.Linq;

public class Explosive : MonoBehaviour {
    public float igniteRadius = 7f, explosiveRadius = 5f, damage = 30f;
    public bool detonated;
    public GameObject scorchPrefab;
    StudioEventEmitter explosionSoundEmitter;
    void Start()
    {
        explosionSoundEmitter = GetComponents<StudioEventEmitter>().FirstOrDefault(x => x.Event.Contains("machineexplosion"));
    }

    
    void onDeath()
    {
        if (!detonated)
        {
            RuntimeManager.PlayOneShot("event:/machineexplosion", transform.position);
            GameObject.Instantiate(scorchPrefab, new Vector3(transform.position.x, -0.24f,  transform.position.z), Quaternion.Euler(90, Random.value * 360, 0));
            ParticleSystemManager.EmitRadial("ExplosiveFlame", transform.position, 40, 40);

            Collider[] colliders = Physics.OverlapSphere(transform.position, igniteRadius, ~(1 << (LayerMask.NameToLayer("Debris") | LayerMask.NameToLayer("Explosive"))), QueryTriggerInteraction.Collide);
            foreach (Collider c in colliders)
            {
                if (c.gameObject != gameObject)
                {
                    Health h = c.GetComponent<Health>();
                    if (h == null || (h != null && !h.dead))
                    {
                        c.gameObject.SendMessage("onIgnite", new IgnitionEvent(gameObject), SendMessageOptions.DontRequireReceiver);

                        if (Vector3.Distance(transform.position, c.transform.position) <= explosiveRadius)
                        {
                            Explosive e = c.GetComponent<Explosive>();
                            if ((e != null && !e.detonated) || e == null)
                            {
                                c.gameObject.SendMessage("onDamage", new EntityDamageEvent(damage / Mathf.Clamp(Vector3.Distance(c.transform.position, transform.position) / 3f, 1f, damage), DamageType.Explosive, gameObject), SendMessageOptions.DontRequireReceiver);
                            }
                        }
                    }
                }
            }

            detonated = true;
        }
    }
}
