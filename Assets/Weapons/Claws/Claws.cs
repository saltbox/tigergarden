﻿using UnityEngine;
using System.Collections;

public class Claws : Weapon {
    float fireTimer, fireThreshold = 1f;
    public float damage = 1f;
    void FixedUpdate()
    {
        fireTimer -= Time.fixedDeltaTime;
        if (target != null && Vector3.Distance(transform.position, target.transform.position) < 2 && fireTimer < 0)
        {
            Fire();
            fireTimer = fireThreshold;
        }
    }

    protected override void Fire()
    {
        target.SendMessage("onDamage", new EntityDamageEvent(damage, DamageType.Impact, gameObject));
        base.Fire();
    }
}
