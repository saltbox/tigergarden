﻿Shader "Custom/Laser"
{
	Properties
	{
		_beamColor("Beam Color", Color) = (1,1,1,1)
		_auraColor("Aura Color", Color) = (1,1,1,1)
		_beamSize("Beam Size", Range(0, 0.5)) = 0.02
		_fadeSize("Fade Size", Range(0, 0.5)) = 0.02

		_AlphaMult("Alpha Multiplier", Range(0, 1)) = 0.5
		_beamAlphaSize("Beam Alpha Size", Range(0, 0.5)) = 0.5
		_fadeAlphaSize("Fade Alpha Size", Range(0, 0.5)) = 0.5

		_beamRandomSize("Beam No Random Size", Range(0, 0.5)) = 0.5
		_fadeRandomSize("Fade Noise Size", Range(0, 0.5)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		Blend SrcAlpha One
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			//Func
			float rand(float3 co)
			{
				return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
			}

			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct fragmentInput
			{
				float2 uv : TEXCOORD0;
				float3 lpos : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			float4 _beamColor;
			float4 _auraColor;

			float _beamSize;
			float _fadeSize;

			float _AlphaMult;
			float _beamAlphaSize;
			float _fadeAlphaSize;

			float _beamRandomSize;
			float _fadeRandomSize;
			
			fragmentInput vert (vertexInput v)
			{
				fragmentInput o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.lpos = v.vertex;
				return o;
			}
			
			fixed4 frag (fragmentInput i) : SV_Target
			{
				float3 randoms = normalize(float3(rand(i.lpos * _Time[1]), rand(i.lpos * _Time[2]), rand(i.lpos * _Time[3])));
				fixed4 col = _beamColor;
				fixed4 aura = _auraColor;

				//aura.a = rand(i.lpos * _Time[1]);

				float dist = abs(i.lpos.x);
				fixed mixC = saturate((dist - _beamSize) / _fadeSize);

				col = lerp(col, aura, mixC);

				fixed mixA = saturate((dist - _beamAlphaSize) / _fadeAlphaSize);
				col.a = lerp(_AlphaMult, 0, mixA);
				
				fixed mixR = saturate((dist - _beamRandomSize) / _fadeRandomSize);
				float random = lerp(1, randoms.x, mixR);
				col.a = col.a * random;	

			//	col.a = clamp(0, 1, col.a);

				return col;
			}

			


			ENDCG
		}
	}
}
