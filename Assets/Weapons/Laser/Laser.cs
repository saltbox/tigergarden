﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;

public class Laser : Weapon {
    public GameObject Beam;
    public float damage = 0.1f;
    public StudioEventEmitter emitter;
    float beamWidth = 1.2f, forwardOffset = 1.2f, hitWidthMultiplier = 0.25f;
    bool firing = false;
    bool soundPlaying = false;
    ParticleSystem fizzleParticleSystem;
    ParticleSystem.EmitParams fizzleParams;
    List<Collider> hits;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        hits = new List<Collider>();
        fizzleParticleSystem = GetComponent<ParticleSystem>();
        fizzleParams = new ParticleSystem.EmitParams();
	}
	
	protected override void Update () {
        /*
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            int mask = ~(1 << LayerMask.NameToLayer("GibBlocker") | 1 << LayerMask.NameToLayer("Ground"));
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, mask))
            {
                //targetList.Add(hit.transform.gameObject);
            }
        }
        targetList.RemoveAll(x => x == null);
        if (targetList.Count != 0)
        {
            target = targetList[0].transform;
        }
        */
        firing = false;
        if (LevelManager.instance.playerActive && Input.GetButton("Fire1") && ammo > 0)
        {
            
            RaycastHit hit;
            if (Physics.SphereCast(Camera.main.ScreenPointToRay(Input.mousePosition),0.25f, out hit, float.PositiveInfinity))
            {
                Vector3 targetPosition = hit.point;
                targetPosition.y = transform.position.y;
                Fire(targetPosition);
            }
        }
        else
        {
            Beam.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        if (firing)
        {
            base.Fire();
        }
        
        hits.Clear();

        if (target != null)
        {
            
        }
        else
        {
            Beam.SetActive(false);
        }
    }

    protected override void Fire(Vector3 targetPoint)
    {
        firing = true;
        if (!soundPlaying) {
            StartCoroutine(PlaySound());
            soundPlaying = true;
        }
        
        //raycast
        Vector3 direction = hitscan(transform.position, targetPoint);
        Beam.SetActive(true);
        RaycastHit hit;
        Beam.transform.rotation = Quaternion.LookRotation(direction);
        if (Physics.CapsuleCast(transform.position + Quaternion.AngleAxis(90, Vector3.up) * direction.normalized * beamWidth * hitWidthMultiplier, transform.position + Quaternion.AngleAxis(-90, Vector3.up) * direction.normalized * beamWidth * hitWidthMultiplier, 0.25f, direction, out hit))
        {
            Beam.transform.position = ((transform.position + transform.forward * forwardOffset) + (transform.position + direction.normalized * hit.distance)) / 2;
            Beam.transform.GetChild(0).transform.localScale = new Vector3(beamWidth, Vector3.Distance(transform.position + transform.forward * forwardOffset, hit.point), beamWidth);
            foreach (RaycastHit h in Physics.SphereCastAll((transform.position + hit.distance * direction.normalized) + Quaternion.AngleAxis(90, Vector3.up) * direction.normalized * beamWidth * hitWidthMultiplier, 0.75f, Quaternion.AngleAxis(-90, Vector3.up) * direction.normalized, beamWidth * hitWidthMultiplier))
            {
                hits.Add(h.collider);
            }
            foreach (Collider h in hits)
            {
                if (h.gameObject.name != transform.root.name)
                {
                    h.gameObject.SendMessage("onDamage", new EntityDamageEvent(damage * Time.deltaTime, DamageType.Laser, gameObject), SendMessageOptions.DontRequireReceiver);
                }
            }
            fizzleParams.position = transform.position + direction.normalized * hit.distance;
            for (int i = 0; i < 7; i++)
            {
                fizzleParams.velocity = new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * 40;
                fizzleParticleSystem.Emit(fizzleParams, 1);
            }
        }
        else
        {
            targetPoint += direction.normalized * 200;
            Beam.transform.position = (transform.position + targetPoint) / 2;
            Beam.transform.GetChild(0).transform.localScale = new Vector3(beamWidth, Vector3.Distance(transform.position, targetPoint), beamWidth);
        }
    }

    IEnumerator PlaySound()
    {
        emitter.Play();
        yield return new WaitWhile(() => { return firing; });
        emitter.Stop();
        soundPlaying = false;
    }
}
