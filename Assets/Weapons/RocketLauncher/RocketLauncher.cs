﻿using UnityEngine;
using System.Collections;

public class RocketLauncher : Weapon {
    public GameObject rocketPrefab, backblastPrefab;
    public float cameraShake = 0.1f;
    public float electrocutionFireChance = 0.1f;
    public GameObject model;

	// Use this for initialization
	protected override void Start () {
	
	}
	
	// Update is called once per frame
    protected override void Update()
    {
        if (LevelManager.instance.playerActive && Input.GetButtonDown("Fire1") && ammo > 0)
        {
            Fire();
        }
        model.transform.localPosition = Vector3.Lerp(model.transform.localPosition, Vector3.zero, 0.1f);
    }

    protected override void Fire()
    {
        base.Fire();
        RaycastHit hit;
        int mask = ~LayerMask.NameToLayer("GibBlocker");
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, mask))
        {
            Vector3 targetPosition = hit.point;
            GameObject newRocket = GameObject.Instantiate(rocketPrefab);
            newRocket.transform.position = transform.position;
            targetPosition.y = transform.position.y;
            newRocket.transform.LookAt(targetPosition);
            newRocket.transform.position += newRocket.transform.forward * 2.5f;

            GameObject newBackblast = GameObject.Instantiate(backblastPrefab);
            newBackblast.transform.position = transform.position;
            newBackblast.transform.LookAt(targetPosition);
            newBackblast.transform.Rotate(Vector3.up, 180);
            newBackblast.transform.position += newBackblast.transform.forward * 2f;

            Camera.main.SendMessage("addShake", cameraShake);
            model.transform.localPosition += new Vector3(0, 0.5f, 0);
        }
    }

    void onElectrocute(ElectrocutionEvent e)
    {
        if (Random.value > 0.9f)
            Fire();
    }
}
