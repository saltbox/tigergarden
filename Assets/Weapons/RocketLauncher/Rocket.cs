﻿using UnityEngine;
using System.Collections;
using FMODUnity;
using System.Linq;

public class Rocket : MonoBehaviour {
    Rigidbody rigidBody;
    float initialAcceleration = 50f, acceleration = 5f, speedLimit = 200f, lifeTimer;
    public float lifetime = 0.7f;
    bool initiallyAccelerated;
    public float cameraShake = 2f;
    public GameObject target;
    StudioEventEmitter rocketSoundEmitter;
    Vector3 lastPosition;
	// Use this for initialization
	void Start () {
        lastPosition = transform.position;
        rigidBody = GetComponent<Rigidbody>();
        lifetime += Random.Range(-lifetime * 0.1f, lifetime * 0.5f);
        rocketSoundEmitter = GetComponents<StudioEventEmitter>().FirstOrDefault(x => x.Event.Contains("rocketlaunch"));
        rocketSoundEmitter.Play();
	}
	
	void FixedUpdate () {
        if (lifeTimer < lifetime)
        {
            lifeTimer += Time.fixedDeltaTime;
            if (!initiallyAccelerated)
            {
                rigidBody.AddForce(transform.forward * initialAcceleration, ForceMode.VelocityChange);
                initiallyAccelerated = true;
            }

            if (target != null)
            {
                Vector3 tempVelocity = rigidBody.velocity;
                tempVelocity.y = 0;
                rigidBody.AddForce(-tempVelocity / 10, ForceMode.VelocityChange);
                transform.LookAt(target.transform.position);
                rigidBody.AddForce(transform.forward * acceleration, ForceMode.VelocityChange);
            }
            else
            {
                if (rigidBody.velocity.magnitude < speedLimit)
                {
                    rigidBody.AddForce(transform.forward * acceleration, ForceMode.VelocityChange);
                }
            }
            for (float i = 0; i < Vector3.Distance(transform.position, lastPosition); i++)
            {
                
                ParticleSystemManager.EmitRadial("RocketSmoke", Vector3.Lerp(lastPosition, transform.position, i / Vector3.Distance(transform.position, lastPosition)), 5f, 1);
            }
        }

        if (lifeTimer >= lifetime)
        {
            rigidBody.useGravity = true;
        }
        lastPosition = transform.position;
	}

    void onDeath()
    {
        transform.position = new Vector3(-9999, -9999, -9999);
        this.enabled = false;
        Camera.main.SendMessage("addShake", cameraShake);
        rocketSoundEmitter.AllowFadeout = false;
        rocketSoundEmitter.Stop();
    }
}
