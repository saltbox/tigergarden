﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class Pistol : Weapon {
    public GameObject bulletPrefab;
    public StudioEventEmitter emitter;
    float fireTimer, fireInterval = 0.08f;

	// Use this for initialization
	protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	protected override void Update() 
    {
        if (LevelManager.instance.playerActive && Input.GetButton("Fire1") && fireTimer <= 0 && ammo >= 1)
        {
            Fire();
            fireTimer = fireInterval;
        }
	}

    protected void FixedUpdate()
    {
        fireTimer -= Time.deltaTime;
    }

    protected override void Fire()
    {
        base.Fire();
        GameObject newBullet = GameObject.Instantiate(bulletPrefab);
        newBullet.transform.position = transform.position + transform.forward;
        newBullet.transform.rotation = transform.rotation;
        newBullet.transform.Rotate(0, (Random.value - 0.5f) * 5f, 0);
        Rigidbody rb = newBullet.GetComponent<Rigidbody>();
        rb.AddForce(newBullet.transform.forward * 90f, ForceMode.VelocityChange);
        rb.AddForce(new Vector3(0, -rb.velocity.y));
        emitter.Play();
        
    }
}
