﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour {
    public static List<GameObject> targetList = new List<GameObject>();
    public Transform target;
    public Vector3 positionToPlayer;
    public string weaponName;
    public int ammo, ammoUnitCost, ammoUnitCount;
    public string ammoName;

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
    }

    protected virtual void Fire()
    {
        ammo -= 1;
    }

    protected virtual void Fire(Vector3 targetPoint)
    {
    }

    protected virtual void AltFire()
    {
    }

    protected Vector3 hitscan(Vector3 origin, Vector3 target)
    {
        Vector3 direction = target - origin;
        return direction;
    }
}
