using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using LibNoise.Generator;
using System.Linq;

public class NodeGraphStructureGeneration : MonoBehaviour {
    public Material groundMaterial;
	public GameObject WallPrefab, GlassPrefab;
    public GameObject machinePrefab, zombiePrefab, playerSpawn;
	public int noiseSeed;
    public int machineCount = 30, zombieCount = 30;
    public float playerClearRadius = 10f;
    [Range(0.0f, 1.0f)]
    public float noiseCullThreshold;
    [Range(0.0f, 1.0f)]
    public float defaultDoorRatio;
    GameObject Level;
    public bool debugIndices, debugDisabled, debugHorizontalEast, debugHorizontalWest, debugVerticalNorth, debugVerticalSouth;
    List<Wall> debugWalls;
	// Use this for initialization
	void Start () {
        
	}

    public Building Generate(Vector3 position, int levelWidth, int levelHeight, int wallLength, bool catwalk = false)
    {
        Building b = new Building(position, levelWidth, levelHeight, wallLength);
        if (catwalk)
        {
            b.walls = GenerateLevel(position, levelWidth, levelHeight, wallLength, 1);
        }
        else
        {
            b.walls = GenerateLevel(position, levelWidth, levelHeight, wallLength, defaultDoorRatio);
        }

        if (!catwalk)
        {
            Populate(position, machineCount, zombieCount, levelWidth, levelHeight, wallLength);
        }

        PositionFloor(position, levelWidth, levelHeight, wallLength, catwalk);
        return b;
    }

    void PositionFloor(Vector3 position, float levelWidth, float levelHeight, int wallLength, bool catwalk)
    {
        GameObject floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
        if (catwalk)
        {
            floor.transform.localScale = new Vector3((levelWidth - 1) * wallLength + 1f, (levelHeight - 1) * wallLength + 1f, 2);
        }
        else
        {
            floor.transform.localScale = new Vector3((levelWidth - 1) * wallLength + 1f, (levelHeight - 1) * wallLength + 1f, 500);
        }
        floor.transform.position = (position + new Vector3(wallLength * levelWidth - wallLength, -0.5f - floor.transform.localScale.z, wallLength * levelHeight - wallLength) + position) / 2;
        floor.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));

        floor.layer = LayerMask.NameToLayer("Ground");
        floor.GetComponent<Renderer>().material = groundMaterial;
        floor.GetComponent<Renderer>().material.color = Color.black;
        floor.name = "Floor";

        GameObject CRFloor = GameObject.CreatePrimitive(PrimitiveType.Plane);
        CRFloor.transform.localScale = new Vector3((levelWidth - 1) * wallLength + 50f, 1, (levelHeight - 1) * wallLength + 50f);
        CRFloor.transform.position = (position + new Vector3(wallLength * levelWidth - wallLength, -0.5f, wallLength * levelHeight - wallLength) + position) / 2;
        CRFloor.layer = LayerMask.NameToLayer("Ground");
        GameObject.Destroy(CRFloor.GetComponent<Renderer>());
        CRFloor.name = "Camera Raycast Floor";
        
    }
    
    List<Wall> GenerateLevel(Vector3 position, int levelWidth, int levelHeight, int wallLength, float doorRatio)
    {
        Random.seed = noiseSeed;
        List<Wall> walls = new List<Wall>();
        //Position walls
		for (int x = 0; x < levelWidth; x += 1) {
			for (int z = 0; z < levelHeight; z += 1) {
				//West-East Wall
				Wall WEWall = new Wall(new Vector3(x, 0, z), new Vector3(x + 1, 0, z), wallLength, false);
				if ((z == 0 || z == levelHeight - 1) && x < (levelWidth) - 1) {
					WEWall.exterior = true;
				}
                if (Random.value > 1 - doorRatio)
                {
                    WEWall.door = true;
                }
				walls.Add(WEWall);
				
				//North-South Wall
				Wall NSWall = new Wall(new Vector3(x, 0, z), new Vector3(x, 0, z + 1), wallLength, true);
                if ((x == 0 || x == levelWidth - 1) && z < (levelHeight) - 1)
                {
					NSWall.exterior = true;
				}
                if (Random.value > 1 - doorRatio)
                {
                    NSWall.door = true;
                }
				walls.Add(NSWall);
			}
		}

        //walls[0].disabled = true;
        //walls[walls.Count - levelHeight * 2 - 2].disabled = true;

		//Connect
		for (int i = 0; i < (levelWidth * levelHeight * 2); i++) {
            Wall w, wc;
            if (WallAt(i, walls, out w, levelWidth, levelHeight))
            {
                //West-East
                if (i % 2 == 0)
                {
                    //West contacts
                    if (WallAt(i + 1, walls, out wc, levelWidth, levelHeight))
                        w.NWContacts.Add(wc);
                    if (WallAt(i - 1, walls, out wc, levelWidth, levelHeight) && i % (levelHeight * 2) != 0)
                        w.NWContacts.Add(wc);
                    if (WallAt(i - (levelHeight * 2), walls, out wc, levelWidth, levelHeight))
                        w.NWContacts.Add(wc);
                    //East contacts
                    if (WallAt(i + (levelHeight * 2), walls, out wc, levelWidth, levelHeight))
                        w.SEContacts.Add(wc);
                    if (WallAt(i + (levelHeight * 2) - 1, walls, out wc, levelWidth, levelHeight) && i % (levelHeight * 2) != 0)
                        w.SEContacts.Add(wc);
                    if (WallAt(i + (levelHeight * 2) + 1, walls, out wc, levelWidth, levelHeight))
                        w.SEContacts.Add(wc);
                }
                //North-South
                else
                {
                    //North contacts
                    if (WallAt(i + 1, walls, out wc, levelWidth, levelHeight) && (i + 1) % (levelHeight * 2) != 0)
                        w.NWContacts.Add(wc);
                    if (WallAt(i + 2, walls, out wc, levelWidth, levelHeight) && (i + 1) % (levelHeight * 2) != 0)
                        w.NWContacts.Add(wc);
                    if (WallAt(i - (levelHeight * 2) + 1, walls, out wc, levelWidth, levelHeight) && (i + 1) % (levelHeight * 2) != 0)
                        w.NWContacts.Add(wc);
                    //South contacts
                    if (WallAt(i - 1, walls, out wc, levelWidth, levelHeight))
                        w.SEContacts.Add(wc);
                    if (WallAt(i - 2, walls, out wc, levelWidth, levelHeight) && (i - 1) % (levelHeight * 2) != 0)
                        w.SEContacts.Add(wc);
                    if (WallAt(i - (levelHeight * 2) - 1, walls, out wc, levelWidth, levelHeight))
                        w.SEContacts.Add(wc);
                }
            }
            
		}

		//Noise Cull
        Random.seed = noiseSeed;
		foreach(Wall w in walls) {
			if (!w.exterior && Random.value > noiseCullThreshold)
            {
				w.disabled = true;
			}
		}

        //Hanging Cull
        foreach (Wall w in walls)
        {
            HangingCull(w);
        }

        debugWalls = walls;
        return walls;
    }

    public void InstantiateWalls(Building b)
    {
        foreach (Wall w in b.walls)
        {
            if (b.walls.Count > 0 && !w.disabled && !b.exits.Contains(w))
            {
                Vector3 targetPosition = Vector3.zero;
                for (float i = 0; i < w.length; i++)
                {
                    targetPosition = new Vector3(Mathf.Lerp(w.p1.x * b.wallLength, w.p2.x * b.wallLength, i / w.length), 0.5f, Mathf.Lerp(w.p1.z * b.wallLength, w.p2.z * b.wallLength, i / w.length));
                    if (Mathf.Abs((w.length / 2) - i) > 2 || w.exterior || !w.door)
                    {

                        if (Mathf.Abs((w.length / 2) - i) <= 3 && w.exterior)
                        {
                            Collider[] col = Physics.OverlapSphere(targetPosition + b.position, 0.25f, ~(1 << LayerMask.NameToLayer("Ground")));
                            if (col.Length < 1)
                            {
                                GameObject newWall = GameObject.Instantiate(GlassPrefab);
                                newWall.transform.position = targetPosition + b.position;
                                //newWall.hideFlags = HideFlags.HideInHierarchy;
                            }
                        }
                        else
                        {
                            Collider[] col = Physics.OverlapSphere(targetPosition + b.position, 0.25f, ~(1 << LayerMask.NameToLayer("Ground")));
                            if (col.Length < 1)
                            {
                                GameObject newWall = GameObject.Instantiate(WallPrefab);
                                newWall.transform.position = targetPosition + b.position;
                                //newWall.hideFlags = HideFlags.HideInHierarchy;
                            }
                        }
                    }
                }
                targetPosition += Vector3.forward;
                if (w.vertical && Physics.OverlapSphere(targetPosition + b.position, 0.25f, ~(1 << LayerMask.NameToLayer("Ground"))).Length < 1)
                {
                    GameObject newWall = GameObject.Instantiate(WallPrefab);
                    newWall.transform.position = targetPosition + b.position;
                    //newWall.hideFlags = HideFlags.HideInHierarchy;
                }
            }
        }
    }
	
	bool WallAt(int i, List<Wall> walls, out Wall w, int levelWidth, int levelHeight) {
        if (i < 0 || i > levelWidth * levelHeight * 2 - 1)
        {
            w = null;
            return false;
        }
        else
        {
            w = walls[i];
            return true;
        }
	}

    void HangingCull(Wall w)
    {
        if (w.disabled || w.exterior)
            return;
        bool SEEnabledContact = false;
        foreach (Wall wc in w.SEContacts)
        {
            if (!wc.disabled)
            {
                SEEnabledContact = true;
            }
        }
        bool NWEnabledContact = false;
        foreach (Wall wc in w.NWContacts)
        {
            if (!wc.disabled)
            {
                NWEnabledContact = true;
            }
        }
        if (!NWEnabledContact || !SEEnabledContact)
        {
            w.disabled = true;
            foreach (Wall wc in w.SEContacts)
                HangingCull(wc);
            foreach (Wall wc in w.NWContacts)
                HangingCull(wc);
        }
    }

    public void Populate(Vector3 position, int machineCount, int zombieCount, int levelWidth, int levelHeight, int wallLength)
    {
        GameObject[] zombies;
        Vector3 bottomLeft = position;
        Vector3 topRight = new Vector3((levelWidth - 1) * wallLength, 0, (levelHeight - 1) * wallLength) + position;
        Vector3 targetPosition;
        for (int i = 0; i < machineCount; i++)
        {
            targetPosition = new Vector3(Random.Range((int)bottomLeft.x, (int)topRight.x), 0.25f, Random.Range((int)bottomLeft.z, (int)topRight.z));
            if (!Physics.CheckSphere(targetPosition, 0.2f, ~LayerMask.NameToLayer("Ground")) && Vector3.Distance(targetPosition, playerSpawn.transform.position) > playerClearRadius)
            {
                GameObject newMachine = GameObject.Instantiate(machinePrefab);
                newMachine.transform.position = targetPosition;
            }
            else
            {
                i--;
            }
        }
        zombies = new GameObject[zombieCount];
        for (int i = 0; i < zombieCount; i++)
        {
            targetPosition = new Vector3(Random.Range((int)bottomLeft.x, (int)topRight.x), 0.25f, Random.Range((int)bottomLeft.z, (int)topRight.z));
            if (!Physics.CheckSphere(targetPosition, 0.2f, ~LayerMask.NameToLayer("Ground")) && Vector3.Distance(targetPosition, playerSpawn.transform.position) > playerClearRadius)
            {
                GameObject newZombie = GameObject.Instantiate(zombiePrefab);
                newZombie.transform.position = targetPosition;
                zombies[i] = newZombie;
            }
            else
            {
                i--;
            }
        }
        GameObject.Find("Goal").transform.position = topRight;
    }

    void OnDrawGizmos()
    {
        int n = 0;
        if (debugWalls == null)
        {
            return;
        }

        foreach (Wall w in debugWalls)
        {
            if (!w.disabled || debugDisabled)
            {
                if (n % 2 == 0)
                {
                    if (debugHorizontalWest)
                    {
                        foreach (Wall wc in w.NWContacts)
                        {
                            if (!wc.disabled || (wc.disabled && debugDisabled))
                                Gizmos.DrawLine((w.p1 + w.p2) / 2, (wc.p1 + wc.p2) / 2);
                        }
                    }

                    if (debugHorizontalEast)
                    {
                        foreach (Wall wc in w.SEContacts)
                        {
                            if (!wc.disabled || (wc.disabled && debugDisabled))
                                Gizmos.DrawLine((w.p1 + w.p2) / 2, (wc.p1 + wc.p2) / 2);
                        }
                    }
                }

                if ((n % 2 != 0))
                {
                    if (debugVerticalNorth)
                    {
                        foreach (Wall wc in w.NWContacts)
                        {
                            if (!wc.disabled || (wc.disabled && debugDisabled))
                                Gizmos.DrawLine((w.p1 + w.p2) / 2, (wc.p1 + wc.p2) / 2);
                        }
                    }

                    if (debugVerticalSouth)
                    {
                        foreach (Wall wc in w.SEContacts)
                        {
                            if (!wc.disabled || (wc.disabled && debugDisabled))
                                Gizmos.DrawLine((w.p1 + w.p2) / 2, (wc.p1 + wc.p2) / 2);
                        }
                    }
                }
            }
            n++;
        }

        if (debugIndices)
        {
            for (int i = 0; i < debugWalls.Count; i++)
            {
                UnityEditor.Handles.Label((debugWalls[i].p1 + debugWalls[i].p2) / 2, i.ToString());
            }
        }
    }
}

public class Wall {
	public Vector3 p1;
	public Vector3 p2;
    public int length;
	public bool exterior, disabled, vertical, door, exit, connected;
	public List<Wall> NWContacts = new List<Wall>(), SEContacts = new List<Wall>();

    public Wall(Vector3 p1, Vector3 p2, int length, bool vertical)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.vertical = vertical;
        this.length = length;
    }

	public Vector3 GetMidpoint() {
		return p1 + p2 / 2;
	}

	public bool isLoose() {
		bool hasNWContact = false, hasSEContact = false;
		foreach(Wall w in NWContacts) {
			if (!w.disabled) {
				hasNWContact = true;
			}
		}
		foreach(Wall w in SEContacts) {
			if (!w.disabled) {
				hasSEContact = true;
			}
		}
		return !(hasNWContact && hasSEContact);
	}
}
