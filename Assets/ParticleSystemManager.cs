﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleSystemManager : MonoBehaviour {
    static Dictionary<string, ParticleSystem> particleSystems;
    static ParticleSystem.EmitParams emitParams;
	// Use this for initialization
	void Start () {
        particleSystems = new Dictionary<string, ParticleSystem>();
        foreach (Transform t in transform)
        {
            particleSystems.Add(t.name, t.GetComponent<ParticleSystem>());
        }
        emitParams = new ParticleSystem.EmitParams();
	}
	
	public static void EmitRadial(string name, Vector3 position, float speed, int count) {
        emitParams.position = position;
        for (int i = 0; i < count; i++)
        {
            emitParams.velocity = new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * speed;
            particleSystems[name].Emit(emitParams, 1);
        }
    }

    public static void EmitConical(string name, Vector3 position, float speed, float radius, Vector3 direction, int count)
    {
        emitParams.position = position;
        for (int i = 0; i < count; i++)
        {
            //Vector3 randomizedDirection =  * radius) * direction;
            emitParams.velocity = Quaternion.Euler(new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * radius) * direction * speed;
            particleSystems[name].Emit(emitParams, 1);
        }
    }
}
