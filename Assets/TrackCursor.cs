﻿using UnityEngine;
using System.Collections;

public class TrackCursor : MonoBehaviour {
    public Transform target;
    public float CameraTracking = 10f, cameraHeight = 21f, trackingSpeed = 0.3f, trackingStrength = 0.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        RaycastHit hit;
        int mask = (1 << LayerMask.NameToLayer("Ground"));
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, mask))
        {
            transform.position = Vector3.Slerp(transform.position, (target.transform.position * (1 - trackingStrength) + hit.point * trackingStrength) + Vector3.up * cameraHeight, trackingSpeed);
        }
        else
        {
            transform.position = Vector3.Slerp(transform.position, target.transform.position + Vector3.up * cameraHeight, trackingSpeed);
        }

        
        
	}

    void SnapToPlayer()
    {
        transform.position = (target.transform.position * trackingStrength) + Vector3.up * cameraHeight;
    }
}
