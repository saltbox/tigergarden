﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class ExposedWire : MonoBehaviour {
    public float ShockRadius;
	// Use this for initialization
	void Start () {
        StartCoroutine("Shock");
	}

    IEnumerator Shock()
    {
        while (true)
        {
            foreach (Collider c in Physics.OverlapSphere(transform.position, ShockRadius))
            {
                c.BroadcastMessage("onElectrocute", new ElectrocutionEvent(gameObject), SendMessageOptions.DontRequireReceiver);
            }
            yield return new WaitForSeconds(1f);
        }
    }

    #if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Handles.color = Color.blue;
        Handles.DrawWireDisc(transform.position, Vector3.up, ShockRadius);
        Handles.color = Color.white;
    }
    #endif
}
