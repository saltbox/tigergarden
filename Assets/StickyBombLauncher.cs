﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StickyBombLauncher : Weapon {
    public GameObject stickyBombPrefab;
    List<GameObject> stickyBombs = new List<GameObject>();
    StickyBombLauncher()
    {
        positionToPlayer = new Vector3(0.467f, 0.016f, 0.22f);
    }

	// Use this for initialization
	protected override void Start () {
	    
	}

    protected override void Fire()
    {
        GameObject[] s = new GameObject[4];
        for (int i = 0; i < s.Length; i++)
        {
            s[i] = GameObject.Instantiate(stickyBombPrefab);
            s[i].transform.position = transform.position + transform.forward;
            s[i].GetComponent<Rigidbody>().AddForce(Quaternion.Euler(0, Random.value * 5 - 2.5f, 0) * transform.forward * 40, ForceMode.VelocityChange);
            stickyBombs.Add(s[i]);
        }
    }

    protected override void AltFire()
    {
        base.AltFire();
        
        foreach (GameObject s in stickyBombs)
        {
            StartCoroutine(SendDetonateMessage(s));
        }
        stickyBombs.Clear();
    }

    IEnumerator SendDetonateMessage(GameObject s)
    {
        yield return new WaitForSeconds(Random.value * 0.2f);
        s.SendMessage("Detonate");
    }
	
	// Update is called once per frame
	protected override void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            AltFire();
        }
	}
}
