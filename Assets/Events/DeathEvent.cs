﻿using UnityEngine;
using System.Collections;

public class DeathEvent : Event {
    public DamageType damageType { get; private set; }
    public DeathEvent(GameObject killer, DamageType damageType)
        :base(killer)
    {
        this.damageType = damageType;
    }
}
