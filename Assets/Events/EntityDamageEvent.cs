﻿using UnityEngine;
using System.Collections;
public enum DamageType { Impact, Fire, Laser, Explosive}
public class EntityDamageEvent : Event
{
    public float damage { get; private set; }
    public DamageType damageType { get; private set; }

    public EntityDamageEvent(float damage, DamageType damageType, GameObject sender)
        :base(sender)
    {
        this.damage = damage;
        this.damageType = damageType;
    }
}
