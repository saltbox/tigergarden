﻿using UnityEngine;
using System.Collections;

public class Event
{
    public GameObject sender { get; private set; }

    public Event (GameObject sender)
    {
        this.sender = sender;
    }
}
