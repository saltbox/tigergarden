﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class RocketBooster : MonoBehaviour
{
    public Rigidbody toBoost;
    Collider toBoostCollider;
    PlayerMovement playerMovement;
    public StudioEventEmitter emitter;
    Vector3 targetMovement;
    float boostForce = 150f;
    float boostTime = 0.13f, boostTimer = 0f;
    bool boosted = false;


    void Start()
    {
        playerMovement = toBoost.GetComponent<PlayerMovement>();
        toBoostCollider = toBoost.GetComponent<Collider>();

    }
    // Update is called once per frame
    void Update()
    {
        boostTimer += Time.deltaTime;
        if (boostTimer >= boostTime)
        {
            targetMovement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized * boostForce;
            if (Input.GetButton("Jump"))
            {
                boosted = true;
                foreach (Collider c in Physics.OverlapSphere(transform.position, 5f))
                {
                    if (c != toBoostCollider)
                    {
                        c.SendMessage("onIgnite", new IgnitionEvent(gameObject), SendMessageOptions.DontRequireReceiver);
                    }
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (boosted)
        {
            if (targetMovement != Vector3.zero)
            {
                emitter.Play();
                playerMovement.collisionDamage = true;
                StartCoroutine(AddForceNextFrame());
                boostTimer = 0;
                playerMovement.PunchThrough(toBoost.position, toBoost.velocity + targetMovement);
                ParticleSystemManager.EmitConical("ExplosiveFlame", toBoost.position, 4f, 100f, -targetMovement.normalized * 5f, 15);
            }
            boosted = false;
        }

    }

    IEnumerator AddForceNextFrame()
    {
        yield return new WaitForFixedUpdate();
        toBoost.AddForce(targetMovement, ForceMode.VelocityChange);
    }
}
