﻿using UnityEngine;
using System.Collections;

public class AudioSourceAutoDestroy : MonoBehaviour {
    AudioSource source;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!source.isPlaying)
        {
            GameObject.Destroy(gameObject);
        }
	}
}
