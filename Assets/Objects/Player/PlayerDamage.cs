﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerDamage : MonoBehaviour {
    Image damageFlashImage;
    Color a = new Color(1f, 1f, 1f, 1f);
    Color b = new Color(1f, 1f, 1f, 0f);
    float alphaTimer, freezeTimer;
    Rigidbody rigidBody;
    void Start()
    {
        damageFlashImage = GameObject.Find("DamageFlashImage").GetComponent<Image>();
        rigidBody = GetComponent<Rigidbody>();
        StartCoroutine(fadeColor());
    }

    public void onDamage(EntityDamageEvent e)
    {
        alphaTimer = 0f;
        if (e.damage >= 5f)
        {
            rigidBody.AddExplosionForce(e.damage * 10f, e.sender.transform.position, 999f, 0f, ForceMode.VelocityChange);
        }
    }

    IEnumerator fadeColor()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            alphaTimer += 0.5f;
            damageFlashImage.color = Color.Lerp(a, b, alphaTimer);
        }
    }

    void onDeath()
    {
        GetComponent<PlayerMovement>().enabled = false;
    }
}
