﻿using UnityEngine;
using System.Collections;

public class PlayerHands : MonoBehaviour {
    GameObject holding;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        if (Input.GetButtonDown("Fire2"))
        {
            if (holding == null)
            {
                int mask = ~(1 << LayerMask.NameToLayer("GibBlocker") | 1 << LayerMask.NameToLayer("Ground"));
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, mask))
                {
                    holding = hit.transform.gameObject;
                    if (holding.GetComponent<Rigidbody>() == null)
                    {
                        Rigidbody newRigidbody = holding.AddComponent<Rigidbody>();
                        newRigidbody.centerOfMass = Vector3.zero;
                        newRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                        newRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
                    }
                    Rigidbody holdingBody = holding.GetComponent<Rigidbody>();
                    hit.transform.SetParent(transform);
                    holding.transform.localPosition = Vector3.up * 1.5f;
                    holdingBody.isKinematic = true;
                    holding.GetComponent<Collider>().enabled = false;
                }
            }
            else
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, ~(1 << LayerMask.NameToLayer("GibBlocker"))))
                {
                    Vector3 direction = (hit.point - holding.transform.position).normalized;
                    direction.y = 0;
                    holding.transform.SetParent(null);
                    holding.transform.position += new Vector3(0, -1.4f, 0) + direction * 2;
                    Rigidbody holdingBody = holding.GetComponent<Rigidbody>();
                    holdingBody.isKinematic = false;
                    holdingBody.useGravity = false;
                    holdingBody.AddForce(direction * 100, ForceMode.VelocityChange);
                    holding.GetComponent<Collider>().enabled = true;
                    holding = null;
                }
            }
        }
    }
}
