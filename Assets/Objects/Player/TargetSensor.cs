﻿using UnityEngine;
using System.Collections;

public class TargetSensor : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Enemy")
        {
            if (!Weapon.targetList.Contains(c.gameObject))
            {
                Weapon.targetList.Add(c.gameObject);
            }
        }
    }
}
