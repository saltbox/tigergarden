﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {
    Rigidbody rigidBody;
    new Collider collider;
    public bool collisionVulnerable = false, collisionDamage = false;
    Vector3 lastPosition;
	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        //DontDestroyOnLoad(gameObject);
	}

	// Update is called once per frame
	void FixedUpdate () {
        if (collisionDamage)
        {
            Debug.DrawLine(transform.position, transform.position + rigidBody.velocity.normalized * Mathf.Log(rigidBody.velocity.magnitude, rigidBody.drag / 2), Color.green);
            PunchThrough(transform.position, rigidBody.velocity);
        }

        if (LevelManager.instance.playerActive)
        {
            Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            rigidBody.AddForce(move.normalized * 2, ForceMode.VelocityChange);
        }

        if (rigidBody.velocity.magnitude < 25) {
            if (collisionVulnerable)
            {
                StartCoroutine(SetVulnerableFalseNextFrame());
            }
            if (collisionDamage)
            {
                StartCoroutine(SetDamageFalseNextFrame());
            }
        }

        if (rigidBody.velocity.magnitude > 40)
        {
            for (float i = 0; i < Vector3.Distance(transform.position, lastPosition); i++)
            {
                ParticleSystemManager.EmitRadial("RocketSmoke", Vector3.Lerp(lastPosition, transform.position, i / Vector3.Distance(transform.position, lastPosition)), 2.5f, 1);
            }
        }
        lastPosition = transform.position;
	}

    IEnumerator SetVulnerableFalseNextFrame()
    {
        yield return new WaitForFixedUpdate();
        collisionVulnerable = false;
    }

    IEnumerator SetDamageFalseNextFrame()
    {
        yield return new WaitForSeconds(0.1f);
        collisionDamage = false;
    }

    public void PunchThrough(Vector3 position, Vector3 velocity)
    {
        RaycastHit[] hits = Physics.SphereCastAll(position, 0.5f, velocity, Mathf.Log(velocity.magnitude, rigidBody.drag / 2));
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider != collider)
            {
                if (velocity.magnitude > 40)
                {
                    hit.collider.gameObject.SendMessage("onDamage", new EntityDamageEvent(15, DamageType.Impact, gameObject), SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    hit.collider.gameObject.SendMessage("onDamage", new EntityDamageEvent(5, DamageType.Impact, gameObject), SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (collisionVulnerable)
        {
            SendMessage("onDamage", new EntityDamageEvent(5f, DamageType.Impact, gameObject));
        }
    }
}
