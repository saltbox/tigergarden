﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class ConcreteBlock : Block {
    
    protected override void Start()
    {
        base.Start();
    }

    protected override void onDeath()
    {
        ParticleSystemManager.EmitRadial("Smoke", transform.position, 5, 5);
        RuntimeManager.PlayOneShot("event:/rockfall", transform.position);
        base.onDeath();
    }
}
