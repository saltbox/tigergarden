﻿using UnityEngine;
using System.Collections;
using FMODUnity;

[RequireComponent(typeof(Health))]
public class Block : MonoBehaviour {

    Rigidbody rigidBody;
    public float deathShake = 1;
    new public Collider collider;
    new public Renderer renderer;
    public StudioEventEmitter emitter;
    MeshRenderer meshRenderer;
    Health h;
    Color startColor;


    // Use this for initialization
    protected virtual void Start () {
        meshRenderer = GetComponent<MeshRenderer>();
        h = GetComponent<Health>();
        startColor = meshRenderer.material.color;
	}

    protected virtual void onDeath()
    {

        StartCoroutine(MoveNextFrame());
        Camera.main.SendMessage("addShake", deathShake);
    }

    protected virtual void onDamage(EntityDamageEvent e)
    {
        meshRenderer.material.color = Color.Lerp(Color.black, startColor, h.health / h.maxHealth);
    }

    IEnumerator MoveNextFrame()
    {
        yield return new WaitForFixedUpdate();
        transform.position += new Vector3(-9999, -100, 0);
    }
}
