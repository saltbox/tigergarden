﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class GlassBlock : Block
{

    protected override void Start()
    {
        base.Start();
    }

    protected override void onDeath()
    {
        ParticleSystemManager.EmitRadial("Smoke", transform.position, 5, 5);
        base.onDeath();
    }
}
