﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class MachineBlock : Block
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void onDeath()
    {
        //RuntimeManager.PlayOneShot("event:/machineexplosion", transform.position);
        ParticleSystemManager.EmitRadial("Smoke", transform.position, 5, 5);
        base.onDeath();
    }
}
