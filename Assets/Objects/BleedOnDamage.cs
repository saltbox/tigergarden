﻿using UnityEngine;
using System.Collections;

public class BleedOnDamage : MonoBehaviour {
    public GameObject bloodGibs;
    public ParticleSystem bloodParticleInstance;
    public int quantity = 6;

	void onDamage(EntityDamageEvent e)
    {
        if (e.damageType == DamageType.Impact || e.damageType == DamageType.Laser)
        {
            if (bloodParticleInstance == null)
            {
                GameObject blood = GameObject.Instantiate(bloodGibs);
                blood.transform.position = transform.position;
                blood.transform.rotation = Quaternion.LookRotation(e.sender.transform.position - transform.position);
                blood.transform.SetParent(transform);
                bloodParticleInstance = blood.GetComponent<ParticleSystem>();
                bloodParticleInstance.Emit(quantity);
            }
            else
            {
                bloodParticleInstance.Emit(quantity);
            }
        }
    }
}
