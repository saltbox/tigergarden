﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OtherSensor : MonoBehaviour {
    public List<GameObject> zombies;
    public List<GameObject> hostiles;
    GameObject target;
	// Use this for initialization
	void Start () {
        zombies = new List<GameObject>();
        hostiles = new List<GameObject>();
    }

    void Update()
    {
        GameObject p = GameObject.Find("Player");
        if (p != null && hostiles.Count < 1)
        {
            hostiles.Add(p);
        }
    }


	
    public void Clean()
    {
        zombies.RemoveAll(x => x == null);
        hostiles.RemoveAll(x => x == null);
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Zombie>() != null)
        {
            zombies.Add(c.gameObject);
        }

        if (c.name == "Player")
        {
            hostiles.Add(c.gameObject);
        }
    }
    void OnTriggerExit(Collider c)
    {
        if (c.GetComponent<Zombie>() != null)
        {
            zombies.Remove(c.gameObject);
        }

        if (c.name == "Player")
        {
            hostiles.Remove(c.gameObject);
        }
    }
}
