﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Zombie : Enemy {
    public GameObject zombieGibs, bloodGibs, ashParticles, smokeCloud;
    public GameObject target;
    public OtherSensor otherSensor;
    public Claws claws;
    Rigidbody rigidBody;
    GameObject player;
    public float acceleration = 2f;
    float perlinXOff, perlinYOff, perlinDOff, walkSinOff;

    protected override void Start()
    {
        base.Start();
        rigidBody = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
        perlinXOff = UnityEngine.Random.value * 200f;
        perlinYOff = UnityEngine.Random.value * 200f;
        perlinDOff = UnityEngine.Random.value * 200f;
        walkSinOff = UnityEngine.Random.value * Mathf.PI;
    }
    
    void FixedUpdate()
    {
        target = null;
        //find hostiles
        foreach (GameObject currentHostile in otherSensor.hostiles)
        {
            if (currentHostile != null && Vector3.Distance(transform.position, currentHostile.transform.position) < 50)
            {
                RaycastHit hit;
                Physics.Raycast(transform.position + (currentHostile.transform.position - transform.position).normalized, currentHostile.transform.position - transform.position, out hit);
                if (hit.collider != null && hit.collider.gameObject.name == "Player")
                {
                    target = hit.collider.gameObject;
                }
            }
        }

        if (target != null)
        {
            //chase
            Vector3 force = Vector3.zero;
            if (Physics.Raycast(transform.position, target.transform.position - transform.position))
            {
                force += (target.transform.position - transform.position).normalized * acceleration;
            }

            //flocking
            foreach (GameObject other in otherSensor.zombies)
            {
                if (other != null)
                {
                    float distance = Vector3.Distance(transform.position, other.transform.position);
                    if (distance < 3 && distance != 0)
                    {
                        force += (transform.position - other.transform.position).normalized * (1f / (distance));
                    }
                }
            }

            //perlin
            force += ZombieWalkPerlin() / 5f;
            rigidBody.AddForce(force, ForceMode.VelocityChange);

            //attack
            if (target.GetComponent<Zombie>() == null)
            {
                claws.target = target.transform;
            }
        }
        else
        {
            if (rigidBody.velocity.magnitude > 1f)
            {
                rigidBody.AddForce(rigidBody.velocity * 0.1f, ForceMode.VelocityChange);
            }

            if (Vector3.Distance(player.transform.position, transform.position) < 100)
            {
                rigidBody.AddForce(ZombieWalkPerlin(), ForceMode.VelocityChange);
            }
        }
    }

    Vector3 ZombieWalkPerlin()
    {
        float directionChangeRate = 1.5f;
        Vector3 v = (new Vector3(Mathf.PerlinNoise(Time.realtimeSinceStartup * directionChangeRate + perlinXOff, 0) - 0.5f, 0, Mathf.PerlinNoise(0, -Time.realtimeSinceStartup * directionChangeRate + perlinYOff) - 0.5f).normalized * Mathf.PerlinNoise(Time.realtimeSinceStartup / 3f + perlinDOff, 0));
        return v * Mathf.Sin(Time.realtimeSinceStartup + walkSinOff);
    }

    void onDamage(EntityDamageEvent e)
    {
        if (e.damageType == DamageType.Explosive)
        {
            rigidBody.AddExplosionForce(e.damage * 10f, e.sender.transform.position, 999, 0f, ForceMode.VelocityChange);
        }
    }

    protected override void onDeath(DeathEvent e)
    {
        base.onDeath(e);
        ParticleSystemManager.EmitRadial("Blood", transform.position, 20, 6);
        ParticleSystemManager.EmitRadial("ZombieGibs", transform.position, 20, 6);
        Camera.main.SendMessage("addShake", 0.8f);
    }

    bool isHostile(GameObject o)
    {
        return o.name == "Player";
    }
}
