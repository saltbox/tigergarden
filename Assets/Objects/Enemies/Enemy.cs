﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Health))]
public class Enemy : MonoBehaviour {

    // Use this for initialization
    protected virtual void Start () {
	
	}

    // Update is called once per frame
    protected virtual void Update () {
	
	}

    protected virtual void onDeath(DeathEvent e)
    {
        GameObject.Destroy(gameObject);
    }

}
