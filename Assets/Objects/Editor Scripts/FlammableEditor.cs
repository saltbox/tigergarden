﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Flammable))]
public class FlammableEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Ignite"))
        {
            ((Flammable)target).gameObject.SendMessage("onIgnite", new IgnitionEvent(((Flammable)target).gameObject));
        }
    }
}
#endif
