﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using System.Linq;

public class Flammable : MonoBehaviour {
    public static List<Flammable> flammableItems;
    public bool onFire;
    public float propagationDelay = 0.2f, damage = 1f, ignitionChance = 1f, ignitionArea = 1f;

    StudioEventEmitter flameSoundEmitter;

    static Flammable()
    {
        flammableItems = new List<Flammable>();
    }

	void Start () {
        flammableItems.Add(this);
        flameSoundEmitter = GetComponents<StudioEventEmitter>().FirstOrDefault(x => x.Event.Contains("gasfire"));
        if (flameSoundEmitter == null)
        {
            Debug.LogError("Fire sound emitter not found!?");
            Debug.LogError(GetComponent<StudioEventEmitter>());
        }
	}

    void FixedUpdate()
    {
        if (onFire)
        {
            SendMessage("onDamage", new EntityDamageEvent(damage, DamageType.Fire, gameObject));
        }
    }

    public void onIgnite()
    {
        onFire = true;
        StartCoroutine("PropagateIgnition");
        StartCoroutine("Burn");
        
        flameSoundEmitter.Play();
    }

    public void onDamage(EntityDamageEvent e)
    {
        if (e.damageType == DamageType.Fire)
        {
            ParticleSystemManager.EmitRadial("Fire", transform.position, 8, 1);
        }
    }

    void onDeath()
    {
        flameSoundEmitter.Stop();
    }

    IEnumerator Burn()
    {
        while (true)
        {
            SendMessage("onDamage", new EntityDamageEvent(damage, DamageType.Fire, gameObject));
            yield return new WaitForSeconds(1);
        }   
    }

    IEnumerator PropagateIgnition()
    {
        yield return new WaitForSeconds(propagationDelay);
        foreach (Flammable f in flammableItems)
        {
            if (f != null && !f.onFire && Vector3.Distance(f.transform.position, transform.position) < ignitionArea && Random.value < f.ignitionChance)
            {
                f.SendMessage("onIgnite", new IgnitionEvent(gameObject));
            }
        }
        StartCoroutine("PropagateIgnition");
    }
}
