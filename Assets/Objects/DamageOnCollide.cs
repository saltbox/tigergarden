﻿using UnityEngine;
using System.Collections;
using System;

public class DamageOnCollide : MonoBehaviour {
    public bool selfDamage = false, otherDamage = true;
    public float damage = 200, velocityThreshold = 0;
	void OnCollisionEnter(Collision c)
    {
        Rigidbody rigidBody = GetComponent<Rigidbody>();
        if (c.relativeVelocity.magnitude >= velocityThreshold)
        {
            if (selfDamage)
            {
                gameObject.SendMessage("onDamage", new EntityDamageEvent(damage, DamageType.Impact, gameObject), SendMessageOptions.DontRequireReceiver);
            }
            if (otherDamage)
            {
                c.collider.SendMessage("onDamage", new EntityDamageEvent(damage, DamageType.Impact, gameObject), SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
