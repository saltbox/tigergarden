﻿using UnityEngine;
using System.Collections;

public class GasPipe : MonoBehaviour {
    public GameObject gasPrefab;
    public float emissionFrequency = 1;
    public int capacity = 50;
    float emissionTimer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        emissionTimer += Time.fixedDeltaTime;
        if (emissionTimer > emissionFrequency && capacity > 0)
        {
            emissionTimer = 0;
            capacity--;
            GameObject.Instantiate(gasPrefab, new Vector3(transform.position.x + Random.Range(-0.25f, 0.25f), -0.5f, transform.position.z + Random.Range(-0.25f, 0.25f)), Quaternion.identity);
        }
	}

    
}
