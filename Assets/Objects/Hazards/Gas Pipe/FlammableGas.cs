﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlammableGas : MonoBehaviour {
    static int gasCount;
    List<Rigidbody> contactingGasRigidbodies = new List<Rigidbody>();
	// Use this for initialization
	void Start () {
        gasCount++;
	}

    void onDeath()
    {
        gasCount--;
        //note: particle parent is nulled before GameObject.Destroy is called, because Destroy also appears to deactivate scripts on child objects.
        if (transform.childCount > 0)
        {
            transform.GetChild(0).SetParent(null);
        }
        GameObject.Destroy(gameObject);
    }

    void OnTriggerEnter(Collider c)
    {

        FlammableGas other = c.GetComponent<FlammableGas>();
        if (other != null)
        {
            contactingGasRigidbodies.Add(other.GetComponent<Rigidbody>());
        }
        
    }

    void OnTriggerExit(Collider c)
    {
        FlammableGas other = c.GetComponent<FlammableGas>();
        if (other != null)
        {
            contactingGasRigidbodies.Remove(other.GetComponent<Rigidbody>());
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
        if ((Random.Range(0, gasCount) > Mathf.Pow(gasCount, 0.78f))) {
            foreach (Rigidbody other in contactingGasRigidbodies)
            {
                if (other != null) {
                    other.AddForce((other.transform.position - transform.position).normalized * Mathf.Clamp(2.5f - Vector3.Distance(other.transform.position, transform.position), 1, float.PositiveInfinity)); 
                }
            }
        }
	}
}
