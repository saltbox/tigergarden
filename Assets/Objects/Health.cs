﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
    public float health = 100;
    public float maxHealth;

    public void Start()
    {
        maxHealth = health;
    }

    public bool dead
    {
        get { return health <= 0; }
    }
    bool deathMessageSent;

    public void onDamage(EntityDamageEvent e)
    {
        health -= e.damage;
        if (health <= 0 && !deathMessageSent)
        {
            gameObject.SendMessage("onDeath", new DeathEvent(e.sender, e.damageType));
            deathMessageSent = true;
        }
    }
}
