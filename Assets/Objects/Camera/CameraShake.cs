﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
    public GameObject origin;
    float shake;
	void addShake(float s)
    {
        shake += s;
    }
	
	// Update is called once per frame
	void Update () {
        float curveShake = Mathf.Pow(shake, 0.5f) / 1.2f;

        if (shake > 0)
        {
            shake = Mathf.Lerp(shake, 0, 0.2f);

        } else
        {
            shake = 0;
        }

        transform.position = origin.transform.position + new Vector3(Random.value * curveShake, Random.value * curveShake, Random.value * curveShake);
	}
}
