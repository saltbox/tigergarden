﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class Teleporter : MonoBehaviour {
    public GameObject toTeleport;
    public TrailRenderer trailRenderer;
    public StudioEventEmitter emitter;
    Vector3 targetMovement;
    float distance = 20;
    float teleportTime = 0.13f, teleportTimer = 0f;
	// Use this for initialization
	void Start () {
        trailRenderer.startWidth = 0f;
	}
	
	// Update is called once per frame
	void Update () {
        teleportTimer += Time.deltaTime;
        if (teleportTimer >= teleportTime)
        {
            targetMovement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized * distance;
            if (Input.GetButton("Jump"))
            {
                if (targetMovement != Vector3.zero)
                {
                    emitter.Play();
                    trailRenderer.startWidth = 1f;
                    StartCoroutine(disableTrailRenderer());
                    Vector3 displacement = Vector3.zero;
                    Vector3 displacementDirection = new Vector3(Random.value * 2 - 1, 0, Random.value * 2 - 1).normalized;
                    while (true)
                    {
                        Collider[] cs = Physics.OverlapSphere(toTeleport.transform.position + targetMovement + displacement, 1f, ~(1 << LayerMask.NameToLayer("Ground")), QueryTriggerInteraction.Ignore);
                        if (cs.Length <= 0)
                        {
                            break;
                        }
                        else
                        {
                            displacement += displacementDirection;
                        }
                    }
                    toTeleport.transform.position += targetMovement + displacement;
                    teleportTimer = 0;
                }
            }
        }
	}

    IEnumerator disableTrailRenderer()
    {
        yield return new WaitForSeconds(0.1f);
        trailRenderer.startWidth = 0f;
    }
}
