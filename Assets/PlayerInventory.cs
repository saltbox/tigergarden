﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class PlayerInventory : MonoBehaviour
{
    public GameObject playerArmature;
    public PlayerAnimator playerAnimator;
    public Text ammoCounter;
    public GameObject RocketLauncherPrefab, LaserCannonPrefab, StickyBombLauncherPrefab, PistolPrefab;
    public List<GameObject> weapons = new List<GameObject>();
    public GameObject HeldWeapon;

    public int cash = 300;
    Weapon WeaponInUse;
	// Use this for initialization
	void Start () {
        ammoCounter = GameObject.Find("BulletAmmoCounter").GetComponent<Text>();
        weapons.Add(GameObject.Instantiate(PistolPrefab));
        weapons.Add(GameObject.Instantiate(RocketLauncherPrefab));
        weapons.Add(GameObject.Instantiate(LaserCannonPrefab));
	}
	
	// Update is called once per frame
	void Update () {
        if (WeaponInUse != null)
        {
            ammoCounter.text = WeaponInUse.ammo.ToString();
        }
        if (LevelManager.instance.playerActive)
        {
            for (int i = 1; i <= 3; i++)
            {
                if (Input.GetButtonDown("Weapon " + i))
                {
                    SetupWeapon(weapons[i - 1]);
                }
            }
        }
	}

    void SetupWeapon(GameObject g)
    {
        weapons.ForEach(x => x.SetActive(false));
        g.SetActive(true);
        HeldWeapon = g;
        g.transform.SetParent(playerArmature.transform);
        playerAnimator.heldObject = g.transform.GetChild(0).gameObject;
        WeaponInUse = g.GetComponent<Weapon>();
        g.transform.localPosition = WeaponInUse.positionToPlayer;
        g.transform.rotation = playerArmature.transform.parent.rotation;
    }

    

    public void SetupWeapon(int index)
    {
        SetupWeapon(weapons[index]);
    }
}
