﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour {
    public Transform leftHand, rightHand;
    Transform leftTarget, rightTarget;
    public GameObject heldObject;
    public Vector3 aimTarget;

    Vector3 lookTarget;
	// Use this for initialization
	void Start () {
        
        
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        int mask = ~(LayerMask.NameToLayer("GibBlocker"));
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, mask, QueryTriggerInteraction.Collide))
        {
            lookTarget = hit.point;
            lookTarget.y = -1;
            transform.LookAt(lookTarget);
            transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);

        }
        

        {
            if (heldObject != null)
            {
                leftTarget = heldObject.transform.FindChild("LeftHandTarget");
            }
            else
            {
                leftTarget = transform.FindChild("LeftHandTarget");
            }
        }
        {
            if (heldObject != null)
            {
                rightTarget = heldObject.transform.FindChild("RightHandTarget");
            }
            else
            {
                rightTarget = transform.FindChild("RightHandTarget");
            }
        }
        
        leftHand.position = leftTarget.position;
        rightHand.position = rightTarget.position;
	}
}
