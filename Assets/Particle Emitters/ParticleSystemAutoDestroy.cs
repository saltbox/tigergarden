﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemAutoDestroy : MonoBehaviour
{
    public float lifetime = 5f;

    public void Start()
    {
        GameObject.Destroy(gameObject, lifetime);
    }
}
