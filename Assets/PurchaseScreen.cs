﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PurchaseScreen : MonoBehaviour {
    List<Text> weaponCounters, weaponLabels;
    Text cashCount;
    List<Weapon> weapons;
    PlayerInventory inventory;
    GameObject player;
	// Use this for initialization
	void Start () {
        weaponCounters = new List<Text>();
        weaponLabels = new List<Text>();
        for (int i = 1; i <= 3; i++)
        {
            weaponCounters.Add(GameObject.Find("Weapon" + i + "CountNumber").GetComponent<Text>());
            weaponLabels.Add(GameObject.Find("Weapon" + i + "CountLabel").GetComponent<Text>());
        }
        cashCount = GameObject.Find("CashCount").GetComponent<Text>();
        player = GameObject.Find("Player");
	}

    void OnEnable()
    {
        StartCoroutine(GetPlayerData());
    }

    IEnumerator GetPlayerData()
    {
        yield return new WaitUntil(() => { return player != null; });
        inventory = player.GetComponent<PlayerInventory>();
        weapons = new List<Weapon>();
        for (int i = 0; i < weaponLabels.Count; i++)
        {
            weapons.Add(inventory.weapons[i].GetComponent<Weapon>());
            weaponLabels[i].text = weapons[i].ammoName + ": ";
            
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            player = GameObject.Find("Player");
        }

        if (player != null && weapons != null)
        {
            cashCount.text = "$" + inventory.cash.ToString();
            for (int i = 0; i < weapons.Count; i++)
            {
                weaponCounters[i].text = weapons[i].ammo.ToString();
                if (Input.GetButtonDown("Weapon " + (i + 1)) && inventory.cash >= weapons[i].ammoUnitCost)
                {
                    weapons[i].ammo += weapons[i].ammoUnitCount;
                    inventory.cash -= weapons[i].ammoUnitCost;
                }
            }
        }
	}

    public void Done()
    {
        LevelManager.instance.SetPlayerActive(true, 0.2f);
        if (inventory.HeldWeapon == null)
        {
            inventory.SetupWeapon(0);
        }
        gameObject.SetActive(false);
    }
}
