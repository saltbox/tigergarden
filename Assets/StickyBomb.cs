﻿using UnityEngine;
using System.Collections;

public class StickyBomb : MonoBehaviour {

    void OnCollisionEnter(Collision c)
    {
        if (!c.transform.gameObject.name.Contains("StickyBomb"))
        {
            transform.SetParent(c.transform);
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    void Detonate()
    {
        SendMessage("onDamage", new EntityDamageEvent(10f, DamageType.Explosive, gameObject));
    }

    void onDeath()
    {
        StartCoroutine(MoveNextFrame());
    }

    IEnumerator MoveNextFrame()
    {
        yield return new WaitForFixedUpdate();
        transform.position += new Vector3(-9999, -100, 0);
    }
}
