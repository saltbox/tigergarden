﻿Shader "Custom/Splatter"
{
	Properties
	{
		
		_MainTex("Albedo", 2D) = "white" {}
		_SplatterTex("Splatter", 2D) = "white" {}
		_SplatMap("Splat Map", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float2 uv_MainTex : TEXCOORD1;
				float2 uv_SplatterTex : TEXCOORD2;
			};

			sampler2D _MainTex;
			sampler2D _SplatterTex;

			sampler2D _SplatMap;

			float4 _MainTex_ST;
			float4 _SplatterTex_ST;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;

				o.uv_MainTex = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv_SplatterTex = TRANSFORM_TEX(v.uv, _SplatterTex);
				
				return o;
			}


			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 splat = tex2D(_SplatMap, i.uv);

				fixed4 blood = tex2D(_SplatterTex, i.uv_SplatterTex);

				fixed4 col = tex2D(_MainTex, i.uv_MainTex);

				col = lerp(blood, col ,splat.r);

				//fixed4 col = fixed4(0,1,0,1);
				return col;
			}
			ENDCG
		}
	}
}
