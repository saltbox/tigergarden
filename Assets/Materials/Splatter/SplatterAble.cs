﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class SplatterAble : MonoBehaviour
{
    Material defaultMaterial;
    public Material splatterMaterial;

    Texture defaultTexture;
    Texture2D splatMap;
    RenderTexture splatted;

    // Use this for initialization
    void Start () {
        defaultMaterial = GetComponent<MeshRenderer>().material;

        defaultTexture = defaultMaterial.GetTexture("_MainTex");
        Vector2 defaultScale = defaultMaterial.GetTextureScale("_MainTex");
        defaultMaterial.SetTextureScale("_MainTex", Vector2.one);

        splatMap = new Texture2D(400, 400); // <-- Resolution Higher looks nicer.
        splatMap.name = gameObject.name + "-SplatMap";

        // Fill splatMap with white ( I might make a shader for this later, it'll be faster on larger images.
        for (int y = 0; y < splatMap.height; y ++) 
	    {
            for (int x = 0; x < splatMap.height; x++) 
		    {
                splatMap.SetPixel(x, y, Color.white);
            }
        }
        // Apply all SetPixel calls
        splatMap.Apply();

        splatterMaterial.SetTexture("_SplatMap", splatMap);
       // splatterMaterial.SetTextureScale("_SplatMap", splatterScale);
        splatterMaterial.SetTextureScale("_MainTex", defaultScale);

        splatted = new RenderTexture(1024, 1024, 0);
        splatted.name = gameObject.name + "-Splatted";

        Graphics.Blit(defaultTexture, splatted, splatterMaterial);

        defaultMaterial.SetTexture("_MainTex", splatted);
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if(updateFrame)
        {
            splatMap.Apply(); // <--- SUPER EXPENSIVE (do as little as possible)
            Graphics.Blit(defaultTexture, splatted, splatterMaterial); // Re-render the splattered texture.
            updateFrame = false;
        }
	}

    bool updateFrame = false;
    public void splatter(Vector2 pos)
    {
        // add black spot, which is then blood in the shader
        splatMap.SetPixel((int)pos.x, (int)pos.y, Color.black);
        updateFrame = true; // <-- cause we want to render the texture max once per frame.
    }

    // Colliding with particles, oh geez
    ParticleCollisionEvent[] collisionEvents;
    void OnParticleCollision(GameObject other)
    {
        if(!other.name.ToLower().Contains("bloodgibs"))
        {
            return;
        }

        ParticleSystem ps = other.GetComponent<ParticleSystem>();

        int safeLength = ps.GetSafeCollisionEventSize();
        collisionEvents = new ParticleCollisionEvent[safeLength];

        int numCollisionEvents = ps.GetCollisionEvents(gameObject, collisionEvents);
        for (int i = 0; i < numCollisionEvents; i ++)
        {
            
            Vector3 pos = collisionEvents[i].intersection;

            RaycastHit hit;

            if (!Physics.Raycast(pos, Vector3.down, out hit)) // <-- I have to raycast to get the Texcoord.... this is kinda dumb.
            {
                continue;
            }
            

            Vector2 PixelUV = hit.textureCoord;
            PixelUV.x *= splatMap.width;
            PixelUV.y *= splatMap.height;


            splatter(PixelUV);
        }
    }



}
